#pragma once

#include "tdataroot.h"
#include <exception>

class TQueue : public TDataRoot {
private:
	int first, last;
public:
	TQueue(int size = DefMemSize) : TDataRoot(size) {
		if(size < 0)
			throw std::exception();
		first = 0;
		last = 0;
		MemType = MEM_RENTER;
		DataCount = 0;
	}
	~TQueue();
	bool IsEmpty() const;
	bool IsFull() const;
	void Put(const TData& val);
	TData Get();
	int IsValid();
	void Print();
	int GetNextIndex();
};