#pragma once

class TJobStream {

	int currentJobId;

public:

	TJobStream() {
		currentJobId = 0;
	}

	int GetNextJobId() {
		currentJobId++;
		return currentJobId - 1;
	}

	int GetCurrentJobId() {
		return currentJobId;
	}

};