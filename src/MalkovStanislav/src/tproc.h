#pragma once

#include "tjobstream.h"
#include "tqueue.h"

#define CHANCE_TO_GEN_JOB 5
#define CHANCE_TO_FINISH_JOB 25

class TProc {

	TJobStream jobStream;
	TQueue queue;

	int tactNum;
	int jobNum;
	int doneJobNum;
	int failedJobNum;
	int idleNum;

public:

	TProc();

	void DoWork();
	void CheckJobDone();
	void GenerateJob();
	void PrintStats();

};