#include <cstdlib>
#include <iostream>
#include "tproc.h"

using namespace std;

TProc::TProc() : queue(5) {
	tactNum = 0;
	jobNum = 0;
	idleNum = 0;
	failedJobNum = 0;
	doneJobNum = 0;
}

void TProc::DoWork() {
	tactNum++;
	GenerateJob();
	CheckJobDone();
}

void TProc::CheckJobDone() {
	if(queue.IsEmpty())
		idleNum++;
	else {
		if(rand() % 100 < CHANCE_TO_FINISH_JOB) {
			int n = queue.Get();
			cout << "��������� �������: " << n << endl;
			doneJobNum++;
		}
	}
}

void TProc::GenerateJob() {
	if(rand() % 100 < CHANCE_TO_GEN_JOB) {
		jobNum++;
		int n = jobStream.GetNextJobId();
		cout << "������������� �������: " << n << endl;
		if(queue.IsFull()) {
			cout << "�� ������ ��������� ������� � �������: " << n << endl;
			failedJobNum++;
		} else {
			queue.Put(n);
			cout << "��������� ������� � �������: " << n << endl;
		}
	}
}

void TProc::PrintStats() {
	cout << "---����������---" << endl;
	cout << "���������� ������: " << tactNum << endl;
	cout << "���������� �������: " << jobNum << endl;
	cout << "���������� ����������� �������: " << doneJobNum << endl;
	cout << "���������� ����������� �������: " << failedJobNum << endl;
	cout << "���������� ������ �������: " << idleNum << endl;
	cout << "������� ���������� ������ �� �������: " << (double) (tactNum - idleNum) / doneJobNum << endl;
}