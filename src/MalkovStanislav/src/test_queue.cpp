#include "tqueue.h"

#include <gtest/gtest.h>

TEST(TQueue, can_create_queue_with_positive_length)
{
	ASSERT_NO_THROW(TQueue q(5));
}

TEST(TQueue, throws_when_create_queue_with_negative_length)
{
	ASSERT_ANY_THROW(TQueue q(-5));
}

TEST(TQueue, can_put_value_in_queue)
{
	TQueue q;
	ASSERT_NO_THROW(q.Put(10));
}

TEST(TQueue, can_get_value_from_queue)
{
	TQueue q;
	q.Put(10);
	ASSERT_NO_THROW(q.Get());
}

TEST(TQueue, can_check_empty)
{
	TQueue q;
	ASSERT_TRUE(q.IsEmpty());
}

TEST(TQueue, can_check_full)
{
	TQueue q(1);
	q.Put(10);
	ASSERT_TRUE(q.IsFull());
}