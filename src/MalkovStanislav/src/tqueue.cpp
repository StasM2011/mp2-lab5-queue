#include <iostream>
#include "tqueue.h"

using namespace std;

TQueue::~TQueue() {
	delete[] pMem;
}

bool TQueue::IsEmpty() const {
	return DataCount == 0;
}			

bool TQueue::IsFull() const {
	return DataCount == MemSize;
}

void TQueue::Put(const TData& val) {
    if(IsFull()) {
		SetRetCode(DataFull);
		return;
	}
	pMem[GetNextIndex()] = val;
	last = GetNextIndex();
	DataCount++;
	SetRetCode(DataOK);
}

int TQueue::GetNextIndex() 
{
	return (first + DataCount) % MemSize;
}

TData TQueue::Get() {
    if(IsEmpty()) {
		SetRetCode(DataEmpty);
        return 0;
	}
	TData result = pMem[first];
	first = (first + 1) % MemSize;
	DataCount--;
	SetRetCode(DataOK);
	return result;
}

int TQueue::IsValid() {
	return RetCode == DataOK;
}

void TQueue::Print() {
	std::cout << DataCount << std::endl;
	std::cout << first << " " << last << std::endl;
	for(int i = first; i < first + DataCount; i++)
		std::cout << pMem[i % MemSize] << " ";
	if(!IsEmpty())
		std::cout << std::endl;
	std::cout << "---" << std::endl;
}