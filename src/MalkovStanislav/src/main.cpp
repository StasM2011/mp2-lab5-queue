#include <iostream>
#include <locale>
#include <Windows.h>
#include "tproc.h"
#include <gtest\gtest.h>

#define TACT_NUM 300
#define DELAY 15

int main(int argc, char **argv) {
	/*setlocale(LC_CTYPE, "Russian");
	TProc proc;
	int i = 0;
	while(i < TACT_NUM) {
		proc.DoWork();
		Sleep(DELAY);
		i++;
	}
	proc.PrintStats();*/

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	getchar();
	return 0;
}