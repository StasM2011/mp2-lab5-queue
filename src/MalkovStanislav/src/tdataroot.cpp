#include "tdataroot.h"

#include <iostream>

TDataRoot::TDataRoot(int Size) {
	MemSize = Size;
	pMem = new TElem[MemSize];
	DataCount = 0;
}

void TDataRoot::SetMem(void *p, int size) {
	p = new TElem[size + 1];
	for(int i = 0; i <= DataCount && i < size; i++)
		((PTElem)p)[i] = pMem[i];
	delete[] pMem;
	pMem = ((PTElem)p);
}